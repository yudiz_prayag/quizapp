import { NgModule } from '@angular/core';
import { canActivate, isNotAnonymous, redirectLoggedInTo, redirectUnauthorizedTo } from '@angular/fire/auth-guard';
import { RouterModule, Routes } from '@angular/router';
import { pipe } from 'rxjs';
import { map } from 'rxjs/operators';
import { AdminComponent } from './content/admin/admin.component';
import { SigninComponent } from './content/admin/pages/signin/signin.component';

export const redirectAnonymousTo = (redirect: any[]) =>
    pipe(isNotAnonymous, map(loggedIn => loggedIn || redirect));

const redirectAnonymousToLogin = () => redirectAnonymousTo(['user/userlogin']); 

const redirectLoggedInToItems = () => redirectLoggedInTo(['admin/home']);
const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['/signun']);

const routes: Routes = [
  { path:"", redirectTo:"user", pathMatch:"full"},

  {path:'signin',component:SigninComponent,...canActivate(redirectLoggedInToItems)},

  {  path: 'admin', 
    component:AdminComponent,
    children:[
      {path:"",
      loadChildren: () => import('./content/admin/admin.module').then(m => m.AdminModule)
      ,...canActivate(redirectUnauthorizedToLogin)
      ,data: {breadcrumb: { skip: true }}
    }]
    ,...canActivate(redirectAnonymousToLogin),
    
  }, 
  { 
    path: 'user', 
    loadChildren: () => import('./content/user/user.module').then(m => m.UserModule) 
    
  },
  
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
