import { NgModule } from '@angular/core';
import { canActivate, redirectLoggedInTo, redirectUnauthorizedTo } from '@angular/fire/auth-guard';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../user/pages/home/home.component';
import { UserloginComponent } from './pages/userlogin/userlogin.component';
import { UserComponent } from './user.component';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['user/userlogin']); //login
const redirectLoggedInToItems = () => redirectLoggedInTo(['user/userquiz']);          //item

const routes: Routes = [
  { path: '', children:[
    {path:"", redirectTo:"home",pathMatch:"full"},
    {path:"home", component:HomeComponent , ...canActivate(redirectLoggedInToItems)},
    {path:"userlogin", component:UserloginComponent, ...canActivate(redirectLoggedInToItems)},
  
  ] },
  
  {
     path: 'userquiz', 
     loadChildren: () => import('./pages/userquiz/userquiz.module').then(m => m.UserquizModule)
,...canActivate(redirectUnauthorizedToLogin)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
