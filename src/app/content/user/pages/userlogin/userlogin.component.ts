import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AuthDataService } from 'src/app/shared/services/auth-data.service';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-userlogin',
  templateUrl: './userlogin.component.html',
  styleUrls: ['./userlogin.component.scss']
})
export class UserloginComponent implements OnInit {

  userform!:FormGroup
  categoryList:any
  loginuser:any

  constructor(
    private fb:FormBuilder,
    private router:Router,
    private _authData:AuthDataService,
    private _auth:AuthService,
    private afs:AngularFirestore
  ) { 
    this.categoryList = this.afs.collection('Category',ref=> ref.where('status','==',true)).valueChanges()    
   
  }

  ngOnInit(): void {
    this.userform = this.fb.group({
      
      name:['',Validators.required],
      email:['',[Validators.required,Validators.email]],
      number:['',[Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      category:['',Validators.required]
    })
  }

  get category(){
    return this.userform.controls['category']
  }
 

  userlog(){

    

    if (this.userform.valid) {
      this._auth.Anonymouslogin().then((result) => {
    
      this._authData.setuser(this.userform.value)
      localStorage.setItem('cetagory',JSON.stringify(this.category.value))
      this.router.navigateByUrl('user/userquiz')
      }).catch((err) => {
        
      });
    }
  }


}
