import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserquizRoutingModule } from './userquiz-routing.module';
import { UserquizComponent } from './userquiz.component';
import { MaterialModule } from "../../../../shared/material/material.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { QuetionindexComponent } from './quetionindex/quetionindex.component';
import { QuetionComponent } from './quetion/quetion.component';
import { CountdownModule } from 'ngx-countdown';
@NgModule({
  declarations: [
    UserquizComponent,
    QuetionindexComponent,
    QuetionComponent
  ],
  imports: [
    CommonModule,
    UserquizRoutingModule,
    MaterialModule,

  
    ReactiveFormsModule,
    FormsModule,
    CountdownModule,
    AngularFireModule.initializeApp(environment.config),
    AngularFirestoreModule,
    AngularFireStorageModule,
  ]
})
export class UserquizModule { }
