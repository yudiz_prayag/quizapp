import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AuthDataService } from 'src/app/shared/services/auth-data.service';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-userquiz',
  templateUrl: './userquiz.component.html',
  styleUrls: ['./userquiz.component.scss']
})
export class UserquizComponent implements OnInit {


  questionList: any = []
  index: number = 0
  id: any
  answer: any = []
  worng:any=[]
  colrIndex: any = []
  lastindex = 0
  category: any = []
  counter: number = 0
  obj: any = {}

  constructor(
    private _auth: AuthService,
    private _authData: AuthDataService,
    private router: Router,
    private afs: AngularFirestore
  ) {
    this.category = JSON.parse(localStorage.getItem('cetagory')!)
    this.afs.collection('QuestionList', ref => ref.where('category', 'in', this.category).where("status", '==', true).limit(10))
      .valueChanges().subscribe(data => {
        this.questionList = data
        this.lastindex = data.length
        for (let i = 0; i < data.length; i++) {
          this.answer[i] = this.questionList[i].answer

        }
      })
    this.id = JSON.parse(localStorage.getItem('userId')!)
    // this.id = this._authData.getUser().subscribe(console.log)

  }

  ngOnInit(): void {
  }

  logout() {
    this._auth.signout().then((result) => {
     

    }).catch((err) => {

    });
  }

  indexs(index: number) {
    this.index = index
  }


  next(event: number) {
    this.index = this.index + event
  }

  changes(event: any) {
    this.colrIndex[event] = true
    console.log(this.colrIndex)
  }

  sendData(event: any) {

    for (let i = 0; i < this.answer.length; i++) {


      let answer = this.answer[i].trim();
      let option = event[i].trim();

      if (option == answer) {
        this.counter++
        this.worng[i]=true
      }else{
        this.worng[i]=false
      }

    }

    // this.id.questions = this.questionList
    // this.id.result = this.counter
    // this._authData.LoginUser.doc(this.id.id).set(this.id) 

    this.obj = {
      qestion: this.questionList,
      result: this.counter,
      userId: this.id,
      total: this.lastindex,
      select_answer:event,
      check : this.worng

    }
    this._authData.setQuiz(this.obj)

    console.log('you result ', this.counter)
    // this.router.navigateByUrl(`user/userquiz/result/${this.id}`)
  }

}
