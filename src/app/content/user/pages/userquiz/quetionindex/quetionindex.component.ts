import { Component, Input, OnInit, Output,EventEmitter, ViewChild } from '@angular/core';
import { CountdownComponent, CountdownConfig, CountdownEvent } from 'ngx-countdown';

@Component({
  selector: 'app-quetionindex',
  templateUrl: './quetionindex.component.html',
  styleUrls: ['./quetionindex.component.scss']
})
export class QuetionindexComponent implements OnInit {

  @ViewChild('cd', { static: false }) private countdown?: CountdownComponent;

  @Input() question:any = []
  @Output() index:EventEmitter<number> = new EventEmitter()
  @Input() color:any=[]
  
  TimerConfig: CountdownConfig
  constructor() { 

    this.TimerConfig = { demand: false, leftTime:1800}
  }

  ngOnInit(): void {
  }

  questionIndex(i:number){
    this.index.emit(i)
  }
  timeFinishd(event:any){
    if (event.action == "done") {
      
      console.log('done')
    }
  }
}
