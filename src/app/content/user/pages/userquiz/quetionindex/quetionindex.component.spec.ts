import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuetionindexComponent } from './quetionindex.component';

describe('QuetionindexComponent', () => {
  let component: QuetionindexComponent;
  let fixture: ComponentFixture<QuetionindexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuetionindexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuetionindexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
