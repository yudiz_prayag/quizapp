import { NgModule } from '@angular/core';
import { canActivate, redirectLoggedInTo, redirectUnauthorizedTo } from '@angular/fire/auth-guard';
import { RouterModule, Routes } from '@angular/router';
import { ResultComponent } from '../result/result.component';
import { UserquizComponent } from './userquiz.component';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['user/home']); //login
const redirectLoggedInToItems = () => redirectLoggedInTo(['user/userquiz/result']);    //item
//
//
const routes: Routes = [
  { path: '', component: UserquizComponent ,...canActivate(redirectUnauthorizedToLogin)}
  ,{path:"result/:id",component:ResultComponent,...canActivate(redirectLoggedInToItems)}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserquizRoutingModule { }
