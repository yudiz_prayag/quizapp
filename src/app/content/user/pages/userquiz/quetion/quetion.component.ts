import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AuthDataService } from 'src/app/shared/services/auth-data.service';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-quetion',
  templateUrl: './quetion.component.html',
  styleUrls: ['./quetion.component.scss']
})
export class QuetionComponent implements OnInit {

  @Input() item: any
  @Input() index:any
  @Input() lastindex:any

 
  @Output() pagination: EventEmitter<number> = new EventEmitter()
  @Output() changes = new EventEmitter()
  @Output() sendData = new EventEmitter()

  color=false
  colorindex:any=[]
  answer:any=[]
  id:any

  constructor(
    private _authdata:AuthDataService,
    private router:Router,
    private _auth:AuthService

  ) {
    this.id = JSON.parse(localStorage.getItem('userId')!)
  }

  

  ngOnInit(): void {

  }



  next(number: number) {
    this.pagination.emit(number)
  }

  test(index:any){
    this.colorindex[index]=true
    this.changes.emit(index)
  }
  save(i:any){
    // this.item
    this.answer[this.index] = i
  
    console.log(this.answer)
  }

  submit(){
    this.sendData.emit(this.answer)
    this._auth.signout().then((result) => {
        localStorage.removeItem('userId')
        localStorage.removeItem('cetagory')
        localStorage.removeItem('result')
      
      this.router.navigateByUrl(`user/userquiz/result/${this.id}`)
    })
    

  }

}
