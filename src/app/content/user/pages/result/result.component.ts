import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute } from '@angular/router';
import { from } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { AuthDataService } from 'src/app/shared/services/auth-data.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {

  userId:any
  quiz:any=[]
  user:any=[]
  total:any
  constructor(
   private route: ActivatedRoute,
   private _authData:AuthDataService,
   private afs: AngularFirestore,
  ) { 
  
  
   
  }
  
  ngOnInit(): void {
    this.route.params.pipe(map(e => e.id)).subscribe(data=>{
      this.userId = data
    })

     this.afs.collection('Quiz',ref => ref.where('userId','==',this.userId)).valueChanges().subscribe((data:any)=>{
       this.quiz = data
       
     })

     this.afs.collection('LoginUser',ref => ref.where('id','==',this.userId)).valueChanges().subscribe((data:any)=>{
      this.user = data
      
    })
    

  }

}
