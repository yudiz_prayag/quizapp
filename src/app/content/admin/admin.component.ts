import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';
import { filter } from "rxjs/operators";
import { BreadcrumbService } from 'xng-breadcrumb';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  constructor(
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    private _auth:AuthService,
  ) {

  }

  ngOnInit() {

  }

  home() {
    this.router.navigateByUrl('admin/home')
  }
  quiz() {
    this.router.navigateByUrl('admin/quiz')
  }
  category(){
    this.router.navigateByUrl('admin/category')
  }
  score(){
    this.router.navigateByUrl('admin/scoreboard')
  }



  logout(){
    this._auth.signout().then((result) => {
      this.router.navigateByUrl('/signin')
    }).catch((err) => {
      
    });
  }


}
