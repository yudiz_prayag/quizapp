import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { HomeComponent } from './pages/home/home.component';
import { SigninComponent } from './pages/signin/signin.component';
import { AngularFireAuthGuard, hasCustomClaim, redirectUnauthorizedTo, redirectLoggedInTo } from '@angular/fire/auth-guard';
import { QuizComponent } from './pages/quiz/quiz.component';
import { ManageCategoryComponent } from './pages/manage-category/manage-category.component';
import { ScoreBoardComponent } from './pages/score-board/score-board.component';




const routes: Routes = [
  {
    path: '', children: [
      { path: '', redirectTo: 'home', pathMatch: "full" },
      {
        path: 'home', component: HomeComponent,
        data: { breadcrumb: 'Add Question'  },
      },
      { path: 'quiz', component: QuizComponent,
      data: { breadcrumb: 'Qestions List' },
      },
      { path: 'category', component: ManageCategoryComponent,
      data: { breadcrumb: 'Manage Category' },
      },
      { path: 'scoreboard', component: ScoreBoardComponent,
      data: { breadcrumb: 'Result' },
      }
    ]
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
