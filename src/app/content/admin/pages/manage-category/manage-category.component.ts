import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthDataService } from 'src/app/shared/services/auth-data.service';
import { ColumnMode } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-manage-category',
  templateUrl: './manage-category.component.html',
  styleUrls: ['./manage-category.component.scss']
})
export class ManageCategoryComponent implements OnInit {
  ColumnMode = ColumnMode;
  categoryForm!: FormGroup
  categoryList: any = []
  respons = true

  constructor(
    private fb: FormBuilder,
    private _authData: AuthDataService
  ) {
    this._authData.getCategory().subscribe(data => {
      this.categoryList = data
      this.respons = false
    })


  }


  ngOnInit(): void {
    this.categoryForm = this.fb.group({
      category_name: ['', Validators.required],
      status: [true]
    })
  }

  add() {
    if (this.categoryForm.valid) {
      this._authData.addCategory(this.categoryForm.value)
      this.categoryForm.reset()
        // console.log(this.categoryForm.value);
        // this.categoryForm.markAsUntouched()
        // this.categoryForm.markAsPristine()
        // this.categoryForm.enable()
   

  
    }
  }


  status(obj: any) {
    obj.status = !obj.status
    this._authData.editCategory(obj.id, obj)
  }

  delete(id: any) {
    this._authData.deleteCategory(id)
  }
}
