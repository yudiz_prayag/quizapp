import { Component, Inject, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthDataService } from 'src/app/shared/services/auth-data.service';

@Component({
  selector: 'app-edit-question',
  templateUrl: './edit-question.component.html',
  styleUrls: ['./edit-question.component.scss']
})
export class EditQuestionComponent implements OnInit {

  editQueForm!:FormGroup
  categoryList:any
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb:FormBuilder,
    private storage: AngularFireStorage,
    private _authData:AuthDataService,
    private afs:AngularFirestore
  ) { 
    this.categoryList = this.afs.collection('Category',ref=> ref.where('status','==',true)).valueChanges()    
  }

  ngOnInit(): void {
    this.editQueForm =  this.fb.group({
      question:[this.data.question],
      imageQue:[this.data.imageQue],
      answer:[this.data.answer],
      category:[this.data.category],
      status:[this.data.status],
      options:this.fb.array([
      ] )
    })
    this.auto()
  }

  get status(){
    return this.editQueForm.controls['status'].value
  }

  get imageQue(){
    return this.editQueForm.controls['imageQue']
  }

  uploadFile(e:any){
    const file = e.target!.files[0];
    console.log(file)
    const filePath =`images/${file.name}`;
    this.imageQue.setValue(filePath)
    const ref = this.storage.ref(filePath);
    const task = ref.put(file);
  
  }

  get options(){
    return this.editQueForm.controls['options'] as FormArray
  }

  auto(){
    for (let i = 0; i < this.data.options.length; i++) {
      this.options.push(this.fb.control(this.data.options[i]))
    }
  }

  add(){
    this.options.push(this.fb.control(""));
  }
  remove(i:number){
    this.options.removeAt(i)
  }

  edit(){

    this._authData.editQuestion(this.data.id, this.editQueForm.value)
  }


}
