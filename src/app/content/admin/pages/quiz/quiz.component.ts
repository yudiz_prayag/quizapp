import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';
import { AuthDataService } from 'src/app/shared/services/auth-data.service';
import { EditQuestionComponent } from './edit-question/edit-question.component';
import { ViewQuestionComponent } from './view-question/view-question.component';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit {

  ColumnMode = ColumnMode;
  @ViewChild(DatatableComponent) table!: DatatableComponent;
  questionData:any
  que:any
  category:any
  respons = true

  constructor(
    private _authData:AuthDataService,
    private dialog:MatDialog
  ) { 
    this._authData.getQuestion().subscribe(data=>{
      this.questionData =data
      this.que= data
      this.category= data
      this.respons = false
    })
  }

  ngOnInit(): void {
  }

 


  status(obj:any){
    obj.status = !obj.status
    this._authData.editQuestion(obj.id,obj)
  }

  deleteQuestion(id:any){
    this._authData.deleteQuestion(id)
  }

 
  editQuestion(obj:any){
    this.dialog.open(EditQuestionComponent,{width: '600px',height:'500px',
    data:obj
  })
  }

  viewQuestion(obj:any){
    this.dialog.open(ViewQuestionComponent,{width: '600px',height:'500px',
    data:obj
  })
  }

  updateFilter(event: any) {
    const val = event.target.value.toLowerCase();

    const temp = this.que.filter( (e:any) =>{
      return e.question.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.questionData = temp;
    this.table.offset = 0;
  }

  select(event:any){
    const val = event.target.value.toLowerCase();

    const category = this.category.filter( (e:any) =>{
      return e.category.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.questionData = category;
    this.table.offset = 0;
  }



}
