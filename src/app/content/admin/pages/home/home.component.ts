import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthDataService } from 'src/app/shared/services/auth-data.service';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

 
  selection=true;
  addQuizForm!:FormGroup
  categoryList:any

  
  constructor(
   
    private fb:FormBuilder,
    private storage: AngularFireStorage,
    private _authData:AuthDataService,
    private afs:AngularFirestore
     ) { 
    this.categoryList = this.afs.collection('Category',ref=> ref.where('status','==',true)).valueChanges()    
  }

  ngOnInit(): void {
    this.addQuizForm = this.fb.group({
      question:["",Validators.required],
      answer:[""],
      category:[""],
      imageQue:[""],
      status:[true],
      options:this.fb.array([
        this.fb.control('')
      ]),
    })
   
  }

  get imageQue(){
    return this.addQuizForm.controls['imageQue']
  }

  uploadFile(e:any){
    const file = e.target!.files[0];
    console.log(file)
    const filePath =`images/${file.name}`;
    this.imageQue.setValue(filePath)
    const ref = this.storage.ref(filePath);
    const task = ref.put(file);
  
  }

  get options(){
    return this.addQuizForm.controls['options'] as FormArray
  }

  add(){
    this.options.push(this.fb.control(""));
  }
  remove(i:number){
    this.options.removeAt(i)
  }



  data(){
    if (this.addQuizForm.valid) {
      console.log(this.addQuizForm.value)
      this._authData.setQuestion(this.addQuizForm.value).then((result) => {
        this.addQuizForm.reset()
        this.addQuizForm.markAsPristine()
        this.addQuizForm.markAsUntouched()
        this.addQuizForm.enable()
      })
    }
  }

  
}
