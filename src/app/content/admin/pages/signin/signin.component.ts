import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  hide = true;
  adminForm!: FormGroup;
  errorMsg:any
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private _auth:AuthService
  ) { }

  ngOnInit(): void {
    this.adminForm = this.fb.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", Validators.required]
    })
  }

  adminLogin() {
    if (this.adminForm.valid) {
      this._auth.signin(this.adminForm.value).then((result) => {
        this.router.navigateByUrl("admin/home");
          console.log(result)
        }).catch((err) => {
         this.errorMsg = err
        });
    }
    else{
      this.errorMsg ="some error in validetion"
    }
  }
}
