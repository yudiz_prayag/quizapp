import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private auth:AngularFireAuth
  ) { 

  }

  signin(obj:any){
    return this.auth.signInWithEmailAndPassword(obj.email,obj.password)
  }

  signout(){
    return this.auth.signOut()
 }

 Anonymouslogin(){
  return this.auth.signInAnonymously()
 }

 crentuser(){
   return this.auth.currentUser
 }

}
