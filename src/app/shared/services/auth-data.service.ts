import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, Query } from '@angular/fire/firestore';
import { BehaviorSubject } from 'rxjs';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthDataService {

  QuestionList!: AngularFirestoreCollection
  category!: AngularFirestoreCollection
  LoginUser:AngularFirestoreCollection
  Quiz:AngularFirestoreCollection
  items: any;


  constructor(
    private afs: AngularFirestore,
    
  ) {
    this.QuestionList = this.afs.collection('QuestionList')
    this.category = this.afs.collection('Category')
    this.Quiz = this.afs.collection('Quiz')
    this.LoginUser = this.afs.collection('LoginUser')
    this.items = this.LoginUser.valueChanges({ idField: 'customID' });
  }

  addCategory(obj: any) {
    return this.category.add(obj)
  }

  deleteCategory(id: any) {
    return this.category.doc(id).delete()
  }

  getCategory() {
    return this.category.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    )
  }

  editCategory(id: any, obj: any){
    return this.category.doc(id).update(obj)
  }

  setQuestion(obj: any) {
    return this.QuestionList.add(obj)
  }



  getQuestion() {
    return this.QuestionList.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    )
  }
  
  deleteQuestion(id: any) {
    return this.QuestionList.doc(id).delete()
  }

  editQuestion(id: any, obj: any) {
    return this.QuestionList.doc(id).update(obj)
  }

  setuser(obj:any){
    // Persist a document id
    const id = this.afs.createId();
    const item = { id, obj };
    this.LoginUser.doc(id).set(item);
    localStorage.setItem('userId',JSON.stringify(id))
 }

 getUser(){
   return this.LoginUser.snapshotChanges().pipe(
     map(a => a.map( a =>{
       const data = a.payload.doc.data()
       const id  = a.payload.doc.id
       return {id,...data};
     }))
   )
 }

 updateUser(id:any,obj:any){
  return this.LoginUser.doc(id).update(obj)
 }

 setQuiz(obj:any){
  return this.Quiz.add(obj)
 }


 getUserQuiz(){
  return this.Quiz.snapshotChanges().pipe(
    map(actions => actions.map(a => {
      const data = a.payload.doc.data();
      const id = a.payload.doc.id;
      return { id, ...data };
    }))
  )
 }

}
