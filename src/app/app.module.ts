import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


//firebase
import { AngularFireModule } from "@angular/fire";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { AngularFireStorageModule, BUCKET } from "@angular/fire/storage";
import { environment } from "../environments/environment";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BreadcrumbModule, BreadcrumbService } from 'xng-breadcrumb';



@NgModule({
  declarations: [
    AppComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    
    AngularFireModule.initializeApp(environment.config),
    AngularFirestoreModule,
    AngularFireStorageModule,

    BreadcrumbModule,

  ],
  providers: [
    BreadcrumbService,
    { provide: BUCKET, useValue: 'gs://quizapp-e303e.appspot.com' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
